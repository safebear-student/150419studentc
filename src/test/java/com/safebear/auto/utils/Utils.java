package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Utils {
    private static final String URL = System.getProperty("url","http://toolslist.safebear.co.uk:8080/");
    private static final String BROWSER = System.getProperty("browser", "chrome");

    public static String getUrl(){
        return URL;
    }

    public static WebDriver getDriver(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/driver/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "src/test/resources/driver/geckodriver.exe");
        System.setProperty("webdriver.edge.driver", "src/test/resources/driver/MicrosoftWebDriver.exe");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("window-size=1366,768");

        switch (BROWSER){
            case "chrome":
                return new ChromeDriver(chromeOptions);
            case "firefox":
                return new FirefoxDriver();
           case "edge":
                return new EdgeDriver();
            case "headless":
                chromeOptions.addArguments("headless","disable-gpu");
                return new ChromeDriver(chromeOptions);
            default:
                return new ChromeDriver(chromeOptions);

        }
    }

}
