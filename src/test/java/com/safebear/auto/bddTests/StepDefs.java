package com.safebear.auto.bddTests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {
    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp() {
        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }


    @Given("^I nav to the login page$")
    public void i_nav_to_the_login_page() throws Throwable {
        // Step 1 action: Go to home
        driver.get(Utils.getUrl());

        // Step 1 result:
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle());

    }


    @When("^I enter login details for '(.+)'$")
    public void i_enter_login_details_for_a_User(String userType) {
//        switch (userType) {
//            case "validUser":
//                loginPage.enterUsername("tester");
//                loginPage.enterPassword("letmein");
//                loginPage.clickSubmitButton();
//                break;
//            case "invalidUser":
//                loginPage.login("hack", "arhhh");
//                break;
//            default:
//                Assert.fail("Either the user supplied is not accounted for, or you've types it wrong");
//                break;
//    }
        String username;
        String password;
        switch (userType) {
            case "validUser":
                username = "tester";
                password = "letmein";
                break;
            case "invalidUser":
                username = "hack";
                password = "letmein";
                break;
            default:
                username = "";
                password = "";
        }
        loginPage.login(username, password);
    }

    @Then("^I can see the following message '(.+)'$")
    public void i_can_see_the_following_message(String message) throws Throwable {
        switch (message) {
            case "Username or Password is incorrect":
                Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle(),"We're on the wrong page");
                Assert.assertTrue(loginPage.getValidationWarningMessage().contains(message));
                break;
            case "Login Successful":
                Assert.assertEquals(toolsPage.getPageTitle(),toolsPage.getExpectedPageTitle(),"We're on the wrong page");
                Assert.assertTrue(toolsPage.getLoginSuccessMessage().equals(message));
                break;
            default:
                Assert.fail("Message was incorrect or scenario wasn't found");
                break;
        }
    }
}



